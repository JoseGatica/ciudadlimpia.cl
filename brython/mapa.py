from browser import alert, document, window, html, console

world_map = document["mapid"]

# Access the leaflet.js API
leaflet = window.L


data = {"maxZoom": 18,
        "attribution": 'Map data © ' \
            '<a href="https://www.openstreetmap.org/">OpenStreetMap' \
            '</a> contributors, <a href="https://creativecommons.org/' \
            'licenses/by-sa/2.0/">CC-BY-SA</a>, ' \
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        "id": 'mapbox.streets'
        }

marker = leaflet.marker([0,0])

def mapclick(ev):
    console.log( 'map clicked')
    coords = ev.latlng
    lat = coords.lat
    lon = coords.lng
    console.log(lat)
    console.log(lon)
    console.log('lat:' + str(lat) + ' | lon:' + str(lon) )
    # Update Display coordinates
    document["latli"].text = f'latitude: {lat}'
    document["lonli"].text = f'longitude: {lon}'
    # move marker
    marker.setLatLng([lat, lon])

    
def draw_map(pos):
    """Get position from window.navigator.geolocation and put marker on the
    map.
    """
    if pos is not None:
        xyz = pos.coords
        lat = xyz.latitude
        lon = xyz.longitude
    else:
        lat = 0
        lon = 0

    # Display coordinates
    ul = html.UL(id="nav")
    ul <= html.LI(f'latitude: {lat}', id='latli')
    ul <= html.LI(f'longitude: {lon}', id='lonli')
    document["coords"] <= ul

    # Create world map
    mymap = leaflet.map('mapid').setView([51.505, -0.09], 2)
    leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        'attribution': '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(mymap);

    # Put marker on map
    #leaflet.marker([lat, lon]).addTo(mymap)
    marker.setLatLng([lat, lon])
    marker.addTo(mymap)

    # bind clicker
    mymap.bind('click', mapclick)

    
def navi(pos):
    document <= "We have your location:"
    draw_map(pos)
def nonavi(error):
    document <= "Your browser doesn't support geolocation"
    draw_map(None)
# Setup
geo = window.navigator.geolocation
if geo:
    geo.getCurrentPosition(navi, nonavi)
else:
    alert('geolocation not supported')
