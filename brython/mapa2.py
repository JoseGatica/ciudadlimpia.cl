import json
from browser import alert, document, window, html, console, ajax, aio

main_map = document["mapid"]

# access leaflet.js API

leaflet = window.L
valdivia_coords=[-39.8142,-73.24585]

# leaflet map config
data = {"maxZoom": 18,
        "attribution": 'Map data © ' \
            '<a href="https://www.openstreetmap.org/">OpenStreetMap' \
            '</a> contributors, <a href="https://creativecommons.org/' \
            'licenses/by-sa/2.0/">CC-BY-SA</a>, ' \
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        "id": 'mapbox.streets'
        }

# create map
themap = leaflet.map('mapid').setView(valdivia_coords, 12)
leaflet.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    'attribution': '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(themap)

# custom marker icons
truck_icon = leaflet.icon({'iconUrl': 'icon_024.png',
                           'iconAnchor': [12,12]})

# create mainmarker
mainmarker = leaflet.marker(valdivia_coords, {'draggable': True})
secondmarker = leaflet.marker(valdivia_coords, {'icon': truck_icon})

# create route
route = leaflet.polyline([valdivia_coords, valdivia_coords], {'color': 'red'}).addTo(themap)
route_done = leaflet.polyline([valdivia_coords, valdivia_coords], {'color': 'blue'}).addTo(themap)

def redraw_route(points_next, points_prev):
    route.setLatLngs(points_next)
    route_done.setLatLngs(points_prev)
    route.redraw()
    route_done.redraw()
    
# load crs for distance calculation
crs = leaflet.CRS.EPSG3857


def mainmarker_move(ev):
    # old coords (not used for now)
    oldcoords = ev.oldLatLng
    oldlat = oldcoords.lat
    oldlng = oldcoords.lng
    # new coords
    coords = ev.latlng
    lat = coords.lat
    lon = coords.lng
    # Update Display coordinates
    document["latli"].text = f'My latitude: {lat:.5f}'
    document["lonli"].text = f'My longitude: {lon:.5f}'
    # update distance
    distance = crs.distance(mainmarker.getLatLng(),secondmarker.getLatLng())
    document['distance'].text = f'distance: {distance:.1f} m'
    
    
def mapclick(ev):
    coords = ev.latlng
    lat = coords.lat
    lon = coords.lng
    # Update Display coordinates
    #document["latli"].text = f'My latitude: {lat:.5f}'
    #document["lonli"].text = f'My longitude: {lon:.5f}'
    # move marker
    mainmarker.setLatLng([lat, lon])
    # update distance
    #distance = crs.distance(mainmarker.getLatLng(),secondmarker.getLatLng())
    #document['distance'].text = f'distance: {distance:.1f} m'
    

async def show_pos_test(pos=valdivia_coords):
    latlng = secondmarker.getLatLng()
    lat = latlng['lat']
    lng = latlng['lng']
    console.log(latlng)
    secondmarker.setLatLng([lat+0.0001, lng+0.0001])

async def show_pos(pos):
    lat = pos[0]
    lng = pos[1]
    if document.getElementById('lattruck') and document.getElementById('lngtruck'):
        document["lattruck"].text = f'Truck latitude: {lat:.5f}'
        document["lngtruck"].text = f'Truck longitude: {lng:.5f}'
    # move marker
    secondmarker.setLatLng([lat, lng])
    # update distance
    if document.getElementById('distance'):
        distance = crs.distance(mainmarker.getLatLng(),secondmarker.getLatLng())
        document['distance'].text = f'distance: {distance:.1f} m'

async def geolocsetup():
    # try to get geolocation
    def draw_mainmarker(pos):
        if pos is not None:
            xyz = pos.coords
            lat = xyz.latitude
            lon = xyz.longitude
        else:
            lat = valdivia_coords[0]
            lon = valdivia_coords[1]

        # display coordinates
        ul = html.UL(id="nav")
        ul <= html.LI(f'My latitude: {lat:.5f}', id='latli')
        ul <= html.LI(f'My longitude: {lon:.5f}', id='lonli')

        # Put marker on map
        mainmarker.setLatLng([lat, lon])
        mainmarker.addTo(themap)
        secondmarker.addTo(themap)
        smLatLng = secondmarker.getLatLng()
        lat = smLatLng['lat']
        lng = smLatLng['lng']
        ul <= html.LI(f'Truck latitude: {lat:.5f}', id='lattruck')
        ul <= html.LI(f'Truck longitude: {lng:.5f}', id='lngtruck')
        distance = crs.distance(mainmarker.getLatLng(),secondmarker.getLatLng())
        ul <= html.LI(f'distance: {distance:.1f} m', id='distance')

        document["coords"] <= ul

        # bind map click action to update main marker
        themap.bind('click', mapclick)

        # bind mainmarker move action
        mainmarker.bind('move', mainmarker_move)
        
    def navgeoloc(pos):
        draw_mainmarker(pos)

    def nonavgeoloc(error):
        draw_mainmarker(None)

    geo = window.navigator.geolocation
    if geo:
        geo.getCurrentPosition(navgeoloc, nonavgeoloc)
    else:
        alert('geolocation not supported')

    
async def main():
    # setyp location
    await geolocsetup()
    # load truck points for simulation
    req = await aio.get("datos.json", format="text")
    console.log(req)
    gpsdata = list()
    gpsdata.append(valdivia_coords)
    if req.response:
        gpsdata = json.loads(req.response)
        console.log(gpsdata)

    i = 0
    max_data = len(gpsdata)
    while True:
        if (i+1)<max_data:
            redraw_route(gpsdata[i:], gpsdata[:i+1])
        await show_pos(gpsdata[i])
        await aio.sleep(2)
        i = (i + 2*1) % max_data



# run
aio.run(main())
